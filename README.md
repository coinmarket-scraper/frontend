# WebScraperFrontend
This project was bootstrapped with Create React App.

Typescript and SCSS.
Based on antd style.
Using redux for manage the state.

## start the backend

follow the steps in the repo: https://gitlab.com/coinmarket-scraper/backend

## First Steps
To get started, run the following commands in the project directory to install dependencies and run the application in local server:
```
yarn
yarn start
```
you need to use a browser extension for bypass cors, because the backend project is not configured for this yet.


## running tests

```
yarn test
```



![Demo](https://media.giphy.com/media/B33JOohyucchLEOOau/giphy.gif)
