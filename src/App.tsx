import React from 'react'
import { Route } from 'react-router-dom'
import ScraperView from './views/scraper/Scraper.view'


const App = () => {
  return (
    <React.Fragment>
      <Route exact path='/' component={ScraperView} />
    </React.Fragment>
  )
}

export default App
