import React from 'react'
import './Scraper.view.scss'
import { Col, Divider, PageHeader } from 'antd';
import ScraperList from '../../components/scraper/table/scraperList.component';
import CreateScraper from '../../components/scraper/create/CreateScraper.component';

const ScraperView = () => {
  return (
    <div>
      <PageHeader
        className="site-page-header-responsive"
        backIcon={false}
        title="CoinScraper"
        subTitle="Tracking your currencies"
      />
      <div className="content">
        <Col span={18}>
          <CreateScraper/>
        </Col>
        <Col span={24}>
          <Divider />
        </Col>
        <ScraperList/>
      </div>
    </div>
  )
}

export default ScraperView;