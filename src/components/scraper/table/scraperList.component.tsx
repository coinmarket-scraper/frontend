import React, { useEffect, useState } from 'react'
import './ScraperList.component.scss'
import { Button, Table, Col, Popconfirm, Row, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { IScraper, IStates } from '../../interfaces';
import ScraperService from '../../../services/scraper/scraperService';
import { addScrapers, deleteScraper, loadingScraper } from '../../../store/actions/scraperActions';
import UpdateScraperForm from '../update/updateScraper.component';

const ScraperList = () => {
  const dispatch = useDispatch();
  const scrapers = useSelector((state: IStates) => state.scraper.scrapers);
  const loading = useSelector((state: IStates) => state.scraper.loading);
  const [visible, setVisible] = useState<boolean>(false);
  const [scraper, setScraper] = useState<IScraper>();

  function getScrapers(): void {
    dispatch(loadingScraper(true));
    ScraperService.getScrapers().subscribe((Testscrapers: IScraper[]) => {
      dispatch(addScrapers(Testscrapers));
      dispatch(loadingScraper(false));
    });
  }

  useEffect(getScrapers, []);

  const handleDelete = (id: number) => {
    dispatch(loadingScraper(true));
    ScraperService.deleteScraper({id}).subscribe(status => {
      if (status) dispatch(deleteScraper(id));
      dispatch(loadingScraper(false));
    });
  }

  const handeUpdate = (scraper: IScraper) => {
    setScraper(scraper);
    setVisible(true);
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id'
    },
    {
      title: 'Moneda',
      dataIndex: 'currency'
    },
    {
      title: 'Último valor leído',
      dataIndex: 'value',
      align: 'right' as 'right'
    },
    {
      title: 'Frecuencia',
      dataIndex: 'frequency',
      align: 'center' as 'center'
    },
    {
      title: 'Fecha de creación',
      dataIndex: 'created_at'
    },
    {
      title: 'Acciones',
      dataIndex: 'actions',
      render: (text: any, record: IScraper) =>
      scrapers.length >= 1 ? (
        <Space size="middle">
          <Button id="updateScraperModalTrigger" onClick={() => handeUpdate(record)}>Editar</Button>
          <Popconfirm title="Seguro que deseas eliminar?" onConfirm={() => handleDelete(record.id)}>
            <Button id="deleteScraperButton" danger>Eliminar</Button>
          </Popconfirm>
        </Space>
      ) : null,
    }
  ];

  const reload = () => {
    getScrapers();
  }

  return (
    <Row justify="space-around"  align='stretch'>
      <Col span={24} >
        <Button className="reload" type="primary" onClick={reload} loading={loading}>
          Actualizar
        </Button>
        <Table columns={columns} dataSource={scrapers} scroll={{ x: 500 }} rowKey="id" size="small" bordered/>
        <UpdateScraperForm
          visible={visible}
          scraper={scraper}
          onCreate={() => {
            setScraper(undefined);
            setVisible(false);
          }}
          onCancel={() => {
            setScraper(undefined);
            setVisible(false);
          }}
        />
      </Col>
    </Row>
  )
}

export default ScraperList;