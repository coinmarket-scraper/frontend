import React from "react";
import { Modal } from "antd";
import { IScraper, IStates } from "../../interfaces";
import { Form, InputNumber, FormItem, SubmitButton } from 'formik-antd';
import { Formik } from 'formik';
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import ScraperService from "../../../services/scraper/scraperService";
import { loadingScraper, updateScraper } from "../../../store/actions/scraperActions";

interface UpdateScraperFormProps {
  visible: boolean;
  scraper?: IScraper;
  onCreate: () => void;
  onCancel: () => void;
}

const UpdateScraperForm: React.FC<UpdateScraperFormProps> = ({
  visible,
  scraper,
  onCreate,
  onCancel,
}) => {

  const ValidationSchema = yup.object().shape({
    frequency: yup.number()
      .required("La frecuencia es requerida")
      .max(60, 'Maximo 60 s')
      .min(10, 'Minimo 10 s')
  });
  const dispatch = useDispatch();
  const loading = useSelector((state: IStates) => state.scraper.loading);
  
  function handleCreate(values: any): void {
    dispatch(loadingScraper(true));
    values['id'] = scraper ? scraper.id : 0;
    ScraperService.updateFrequencyScraper(values).subscribe(data => {
      dispatch(updateScraper(data, scraper ? scraper.id : 0));
      onCreate();
    }).add(() => dispatch(loadingScraper(false)));
  }

  return (
    <Modal
      visible={visible}
      title={`Actualizando ${scraper?.currency}`}
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      footer={null}
    >
      <Formik
      onSubmit={(values, {resetForm}) => {
        handleCreate(values);
        resetForm();
      }}
      initialValues={{ frequency: 0 }}
      validationSchema={ValidationSchema}
    >
      {(formik) => {
        const { isValid, dirty } = formik;
        return  (
          <Form layout='inline'>
            <FormItem name="frequency" label="Frecuencia (segundos)" >
              <InputNumber name="frequency" id="updateScraperFrequency"/>
            </FormItem>
            <SubmitButton 
              type="primary" 
              disabled={!(dirty && isValid)} 
              loading={loading}
              id="updateScraperButton"
            >
              actualizar
            </SubmitButton>
          </Form>
        )
      }}
    </Formik>
    </Modal>
  );
};

export default UpdateScraperForm;
