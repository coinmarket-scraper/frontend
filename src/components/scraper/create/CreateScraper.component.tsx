import React from 'react'
import './CreateScraper.component.scss'
import { useDispatch, useSelector } from 'react-redux';
import { IScraper, IStates } from '../../../components/interfaces';
import ScraperService from '../../../services/scraper/scraperService';
import { addScraper, loadingScraper } from '../../../store/actions/scraperActions';
import { Form, Input, InputNumber, FormItem, SubmitButton } from 'formik-antd';
import { Formik } from 'formik';
import * as yup from "yup";

const CreateScraper = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: IStates) => state.scraper.loading);
  const scrapers = useSelector((state: IStates) => state.scraper.scrapers);
  
  const ValidationSchema = yup.object().shape({
    frequency: yup.number()
      .required("La frecuencia es requerida")
      .max(60, 'Maximo 60 s')
      .min(10, 'Minimo 10 s'),
    currency: yup.string()
      .required('El nombre de la moneda es requerido')
      .notOneOf(scrapers.map(scraper => scraper.currency), 'esta moneda ya existe.')
  });

  function createScraper(values: any) {
    dispatch(loadingScraper(true));
    ScraperService.createScraper(values).subscribe((scraper: IScraper) => {
      dispatch(addScraper(scraper));
      dispatch(loadingScraper(false));
    });
  }
  
  return (
    <Formik
      onSubmit={createScraper}
      initialValues={{ currency: '', frequency: 0 }}
      validationSchema={ValidationSchema}
    >
      {(formik) => {
        const { isValid, dirty, setFieldValue } = formik;
        return  (
          <Form layout='inline'>
            <FormItem name="frequency" label="Frecuencia (segundos)">
              <InputNumber name="frequency" id="createScraperFrequency"/>
            </FormItem>
            <FormItem name="currency" label="Moneda">
              <Input 
                onChange={e => setFieldValue('currency', e.target.value.toLowerCase())} 
                name="currency"
                placeholder="bitcoin" 
                id="createScraperCurrency"
              />
            </FormItem>
            <SubmitButton 
              type="primary" 
              disabled={!(dirty && isValid)} 
              loading={loading}
              id="createScraperButton"
            >
              Crear
            </SubmitButton>
          </Form>
        )
      }}
    </Formik>
      

  )
}

export default CreateScraper