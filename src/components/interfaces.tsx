export interface IScraper {
  id: number,
  currency: string,
  value: string, 
  frequency: string,
  created_at: string
}

export interface IScraperState {
  scrapers: IScraper[],
  loading: boolean
}

export interface IStates {
  scraper: IScraperState
}