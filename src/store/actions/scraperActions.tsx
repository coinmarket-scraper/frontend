import { IScraper } from '../../components/interfaces';
import { ADD_SCRAPER, ADD_SCRAPERS, DELETE_SCRAPER, LOADING_SCRAPER, ScraperActionTypes, UPDATE_SCRAPER } from '../types/scraperTypes';

export function addScraper(scraper: IScraper): ScraperActionTypes {
  return {
    type: ADD_SCRAPER,
    payload: scraper
  }
}

export function updateScraper(scraper: IScraper, id: number): ScraperActionTypes {
  return {
    type: UPDATE_SCRAPER,
    payload: scraper,
    id: id
  }
}

export function addScrapers(scrapers: IScraper[]): ScraperActionTypes {
  return {
    type: ADD_SCRAPERS,
    payload: scrapers
  }
}

export function deleteScraper(id: number): ScraperActionTypes {
  return {
    type: DELETE_SCRAPER,
    id: id
  }
}

export function loadingScraper(loading: boolean): ScraperActionTypes {
  return {
    type: LOADING_SCRAPER,
    loading: loading
  }
}