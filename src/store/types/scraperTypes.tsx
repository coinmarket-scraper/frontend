import { IScraper } from "../../components/interfaces";

export const ADD_SCRAPER = 'ADD_SCRAPER';
export const ADD_SCRAPERS = 'ADD_SCRAPERS';
export const DELETE_SCRAPER = 'DELETE_SCRAPER';
export const UPDATE_SCRAPER = 'UPDATE_SCRAPER';
export const LOADING_SCRAPER = 'LOADING_SCRAPER';

interface AddScraperAction {
  type: typeof ADD_SCRAPER
  payload: IScraper
}

interface DeleteScraperAction {
  type: typeof DELETE_SCRAPER
  id: number
}

interface UpdateScraperAction {
  type: typeof UPDATE_SCRAPER
  payload: IScraper
  id: number
}

interface AddScrapersAction {
  type: typeof ADD_SCRAPERS
  payload: IScraper[]
}

interface LoadingScraperAction {
  type: typeof LOADING_SCRAPER
  loading: boolean
}

export type ScraperActionTypes = AddScraperAction | DeleteScraperAction |
 UpdateScraperAction | AddScrapersAction | LoadingScraperAction;