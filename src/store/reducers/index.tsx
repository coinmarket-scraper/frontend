import { combineReducers } from 'redux';
import { scraperReducer } from './scrapers';

export default combineReducers({
  scraper: scraperReducer,
})

