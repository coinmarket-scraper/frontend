import { IScraperState, IScraper } from '../../components/interfaces';
import { ADD_SCRAPER, ADD_SCRAPERS, DELETE_SCRAPER, LOADING_SCRAPER, ScraperActionTypes, UPDATE_SCRAPER } from '../types/scraperTypes';

const initialState: IScraperState = {
  scrapers: [],
  loading: false
}

export function scraperReducer(
  state = initialState,
  action: ScraperActionTypes
): IScraperState {
  switch (action.type) {
    case ADD_SCRAPER:
      return {
        ...state,
        scrapers: [...state.scrapers, action.payload]
      }
    case UPDATE_SCRAPER:
      let { id, payload } = action;
      let scrapers: IScraper[] = [...state.scrapers];
      let itemIndex = state.scrapers.findIndex(scraper => scraper.id === id);
      scrapers[itemIndex] = payload;
      return {
        ...state,
        scrapers,
      }
    case DELETE_SCRAPER:
      return {
        ...state,
        scrapers: state.scrapers.filter(
          scraper => scraper.id !== action.id 
        )
      }
    case ADD_SCRAPERS:
      return {
        ...state,
        scrapers: action.payload
      }
    case LOADING_SCRAPER:
      return {
        ...state,
        loading: action.loading
      }
    default:
      return state
  }
}
