import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { render, waitFor } from '@testing-library/react';
import { fireEvent } from '@testing-library/dom';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import '../../setupTest';
import ScraperList from '../../components/scraper/table/scraperList.component';
import { act } from 'react-dom/test-utils';

const scrapersData = {
  scrapers: [
    {
      id: 1,
      currency: 'bitcoin',
      value: '$ 10.000.00', 
      frequency: '60',
      created_at: '2020-10-09'
    },
    {
      id: 2,
      currency: 'ethereum',
      value: '$ 400.00', 
      frequency: '60',
      created_at: '2020-10-09'
    }
  ]
}

describe('<ScraperList />', () => {
  let wrapper: any;
  let mock = new MockAdapter(axios);
  const mockStore = configureStore();
  const store = mockStore({
    scraper: {
      loading: false,
      scrapers: []
    }
  })

  beforeEach(() => {
    const { container } = render(
      <Provider store={store}>
        <ScraperList />
      </Provider>
    )
    wrapper = container;
  })

  afterEach(() => {
    wrapper = null;
    mock.reset();
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('load scrapers in table', async() => {
    mock.onGet().replyOnce(200, scrapersData);
    await waitFor(() => {
      expect(mock.history.get.length).toEqual(1);
    });
    waitFor(() => { 
      expect(wrapper.querySelector('tr#data-row-key')).toBeInTheDocument();
      expect(wrapper.querySelectorAll('tr#data-row-key').length).toEqual(2);
    });  
  });
});