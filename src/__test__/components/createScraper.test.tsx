import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import CreateScraper from '../../components/scraper/create/CreateScraper.component';
import { render, waitFor } from '@testing-library/react';
import { fireEvent } from '@testing-library/dom';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import '../../setupTest';



describe('<CreateScraper />', () => {
  let wrapper: any;
  let mock = new MockAdapter(axios);
  const mockStore = configureStore();
  const store = mockStore({
    scraper: {
      loading: false,
      scrapers: [{
        id: 1,
        currency: 'bitcoin',
        value: '$ 1000.00', 
        frequency: '60',
        created_at: '2020-10-09'
      }]
    }
  })

  beforeEach(() => {
    const { container } = render(
      <Provider store={store}>
        <CreateScraper />
      </Provider>
    )
    wrapper = container;
  })

  afterEach(() => {
    wrapper = null;
    mock.reset();
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('test submit', async() => {

    mock.onPost().replyOnce(201, {value: 1000});
    const frequency = wrapper.querySelector('input#createScraperFrequency');
  
    await waitFor(() => {
      fireEvent.change(frequency, {
        target: {
            value: 10,
        },
      });
    });

    const currency = wrapper.querySelector('input#createScraperCurrency');

    await waitFor(() => {
      fireEvent.change(currency, {
        target: {
            value: 'bitcoin',
        },
      });
    });
    
    const form: any = wrapper.querySelector('button#createScraperButton'); 
    await act(async () => {
      fireEvent.click(form);
    });
    waitFor(() => {
      expect(mock.history.post.length).toEqual(1);
    });
  });
});