import { scraperReducer } from "../../store/reducers/scrapers";
import { ScraperActionTypes } from "../../store/types/scraperTypes";

const addScraperResponse = {
  id: 1,
  currency: 'bitcoin',
  value: '$ 1000.00', 
  frequency: '60',
  created_at: '2020-10-09'
}

const addScraperReducerData = {
  id: 1,
  currency: 'bitcoin',
  value: '$ 1000.00', 
  frequency: '60',
  created_at: '2020-10-09'
}

const addScrapersResponse = [
  {
    id: 1,
    currency: 'bitcoin',
    value: '$ 1000.00', 
    frequency: '60',
    created_at: '2020-10-09'
  }
]
const AddscrapersReducerData = [
  {
    id: 1,
    currency: 'bitcoin',
    value: '$ 1000.00', 
    frequency: '60',
    created_at: '2020-10-09'
  }
]

describe('scraper reducer', () => {
  it('scraper  reducer expected state', () => {
    const action: ScraperActionTypes = {
      payload: addScraperReducerData,
      type: "ADD_SCRAPER",
    };
    const updatedState = scraperReducer(undefined, action);
    expect(updatedState.scrapers).toHaveLength(1);
    expect(updatedState.scrapers).toEqual([addScraperResponse]);
  });

  it('scrapers  reducer expected state', () => {
    const action: ScraperActionTypes = {
      payload: AddscrapersReducerData,
      type: "ADD_SCRAPERS",
    };
    const updatedState = scraperReducer(undefined, action);
    expect(updatedState.scrapers).toHaveLength(1);
    expect(updatedState.scrapers).toEqual(addScrapersResponse);
  });

  it('delete scraper reducer expected state', () => {
    const action: ScraperActionTypes = {
      payload: AddscrapersReducerData,
      type: "ADD_SCRAPERS",
    };
    const updatedState = scraperReducer(undefined, action);
    expect(updatedState.scrapers).toHaveLength(1);
    expect(updatedState.scrapers).toEqual(addScrapersResponse);

    const deleteAction: ScraperActionTypes = {
      type: "DELETE_SCRAPER",
      id: AddscrapersReducerData[0].id
    }
    const updatedStateTwo = scraperReducer(undefined, deleteAction);
    expect(updatedStateTwo.scrapers).toHaveLength(0);
  });

  it('update scraper reducer expected state', () => {
    const action: ScraperActionTypes = {
      payload: AddscrapersReducerData,
      type: "ADD_SCRAPERS",
    };
    const updatedState = scraperReducer(undefined, action);
    expect(updatedState.scrapers).toHaveLength(1);
    expect(updatedState.scrapers).toEqual(addScrapersResponse);

    const tempScraper = AddscrapersReducerData[0];
    tempScraper.frequency = '50';
    const updateAction: ScraperActionTypes = {
      type: "UPDATE_SCRAPER",
      id: AddscrapersReducerData[0].id,
      payload: tempScraper
    }
    const updatedStateTwo = scraperReducer(updatedState, updateAction);
    expect(updatedStateTwo.scrapers).toHaveLength(1);
    expect(updatedStateTwo.scrapers[0].frequency).toEqual(tempScraper.frequency);
  });
});