
import configureStore from 'redux-mock-store';
import { addScraper, addScrapers, deleteScraper, loadingScraper, updateScraper } from '../../store/actions/scraperActions';


const addScraperResponse = {
  id: 1,
  currency: 'bitcoin',
  value: '$ 1000.00', 
  frequency: '60',
  created_at: '2020-10-09'
}

const addScraperReducerData = {
  id: 1,
  currency: 'bitcoin',
  value: '$ 1000.00', 
  frequency: '60',
  created_at: '2020-10-09'
}

const addScrapersResponse = [
  {
    id: 1,
    currency: 'bitcoin',
    value: '$ 1000.00', 
    frequency: '60',
    created_at: '2020-10-09'
  }
]
const AddscrapersReducerData = [
  {
    id: 1,
    currency: 'bitcoin',
    value: '$ 1000.00', 
    frequency: '60',
    created_at: '2020-10-09'
  }
]

describe("scraper actions", () => {
  const mockStore = configureStore();
  const reduxStore = mockStore();

  beforeEach(() => {
    reduxStore.clearActions();
  });

  describe('add scraper action', () => {
    it('should dispatch the add scraper action', () => {
      const expectedActions = [
        {
          payload: addScraperResponse,
          type: 'ADD_SCRAPER',
        },
      ];
      reduxStore.dispatch(addScraper(addScraperReducerData));
      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });

  describe('add scrapers action', () => {
    it('should dispatch the add scrapers action', () => {
      const expectedActions = [
        {
          payload: addScrapersResponse,
          type: 'ADD_SCRAPERS',
        },
      ];
      reduxStore.dispatch(addScrapers(AddscrapersReducerData));
      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });

  describe('delete scraper action', () => {
    it('should dispatch the delete scraper action', () => {
      const expectedActions = [
        {
          id: 1,
          type: 'DELETE_SCRAPER',
        },
      ];
      reduxStore.dispatch(deleteScraper(1));
      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });

  describe('update scraper action', () => {
    it('should dispatch the update scraper action', () => {
      const expectedActions = [
        {
          id: 1,
          payload: addScraperResponse,
          type: 'UPDATE_SCRAPER',
        },
      ];
      reduxStore.dispatch(updateScraper(addScraperReducerData, 1));
      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });

  describe('loading scraper action', () => {
    it('should dispatch the loading scraper action', () => {
      const expectedActions = [
        {
          loading: true,
          type: 'LOADING_SCRAPER',
        },
      ];
      reduxStore.dispatch(loadingScraper(true));
      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });

})