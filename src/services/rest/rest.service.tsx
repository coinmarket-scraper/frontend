import axios from 'axios';
import { Observable } from 'rxjs';
import { IScraper } from '../../components/interfaces';
import { Config } from '../../config/config';

class RestService {
  static instance: RestService;

  static getInstance = () => {
    if (!RestService.instance) {
      RestService.instance = new RestService();
    }
    return RestService.instance;
  }

  static get(uri: string) {
    return RestService.getInstance().get(uri);
  }

  get = (uri: string) => {
    return new Observable(observe => {
      axios.get(`${Config.API}/${uri}`, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static post = (uri: string, data: any) => {
    return RestService.getInstance().post(uri, data);
  }

  post = (uri: string, data: any) => {
    return new Observable(observe => {
      axios.post(`${Config.API}/${uri}`, data, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static delete = (uri: string, data?: any) => {
    return RestService.getInstance().delete(uri, data);
  }

  delete = (uri: string, data?: any) => {
    return new Observable(observe => {
      axios.delete(`${Config.API}/${uri}`, {
        headers: this.getHeaders(),
        data: data
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static put = (uri: string, data: IScraper) => {
    return RestService.getInstance().put(uri, data);
  }

  put = (uri: string, data: IScraper) => {
    return new Observable(observe => {
      axios.put(`${Config.API}/${uri}`, data, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  getHeaders = () => {
    let headers = {
      "Content-Type": "application/json"
    };
    return headers;
  }
}

export default RestService;