import { IScraper } from "../../components/interfaces";
import RestService from "../rest/rest.service";
import { Observable } from 'rxjs';
import moment from "moment";

class ScraperService {

  static currencyFormat(num: number) {
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
 }
  
  static createScraper = (scraper: any): Observable<IScraper> => {
    return new Observable(observe => {
      try {
        RestService.post(`scrapers/`, scraper).subscribe((data: any) => {
          const scraper: IScraper = data?.data;
          scraper.created_at = moment(scraper.created_at).format('YYYY-MM-DD');
          scraper.value = ScraperService.currencyFormat(Number(scraper.value));
          observe.next(scraper);
          observe.complete();
        })
      } catch (error) {
        observe.next(undefined);
        observe.complete();
      }
    })
  }

  static updateFrequencyScraper = (scraper: any): Observable<IScraper> => {
    return new Observable(observe => {
      try {
        RestService.put(`scrapers/`, scraper).subscribe((data: any) => {
          const scraper: IScraper = data?.data;
          scraper.created_at = moment(scraper.created_at).format('YYYY-MM-DD');
          scraper.value = ScraperService.currencyFormat(Number(scraper.value));
          observe.next(scraper);
          observe.complete();
        })
      } catch (error) {
        observe.next(undefined);
        observe.complete();
      }
    })
  }

  static getScrapers = (): Observable<IScraper[]> => {
    return new Observable(observe => {
      try {
        RestService.get(`scrapers/`).subscribe((data: any) => {
          const scrapers: IScraper[] = data?.data?.scrapers.map((scraper: IScraper) => {
            scraper.created_at = moment(scraper.created_at).format('YYYY-MM-DD');
            scraper.value = ScraperService.currencyFormat(Number(scraper.value));
            return scraper
          });
          observe.next(scrapers);
          observe.complete();
        })
      } catch (error) {
        observe.next(undefined);
        observe.complete();
      }
    })
  }

  static deleteScraper = (scraper: any): Observable<boolean> => {
    return new Observable(observe => {
      try {
        RestService.delete(`scrapers/`, scraper).subscribe((data: any) => {
          observe.next(data?.status === 204 ? true : false);
          observe.complete();
        })
      } catch (error) {
        observe.next(false);
        observe.complete();
      }
    })
  }

}

export default ScraperService;